# endeavor

An Interview Question, Daily. Explained.

## Overview

Here's a sobering fact: I suck at interview questions.

I've worked in complex, distributed system, micro services topping 35+ (with contributions to the majority) and dealt with bugs, issues at scale but, for some reason, getting asked to reverse a LinkedList or explain and implement a HashMap causes the idiot portion of my brain to take over. It's actually quite thrilling - someone asking you a simple programming question, and your mind going blank. It's the same feeling you get when you're speaking and suddenly you forget the next word you want to say, and you're in front of 11 people; 7 of which you've just met.

So what's there left to do?

Brute force, my friends. Brute. Fucking. Force.

I am going to take on increasingly hard interview questions based on the top interview-question-platforms (neat, a new market) or I'll make one up based on things that tend to hang around in my mind.

Most of the solutions will (probably) be in go unless I suddenly have a change of heart. I'll include the code in a single `main.go` as well as a `README.md` explaining the question,  implementation, and O() of solution.

So, without further ado...


*One last precondition: I very well might be wrong. I make no guarantees about being right, or always having the right answer. As previously stated, I'm kinda an dummy. So feel free to @ me with corrections or general verbal abuse. It comes with the space.*
