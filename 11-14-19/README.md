# 11-14-2019

## The Problem


> Given a string, find the length of the longest substring without repeating characters. Can you find a solution in linear time?

```python
class Solution:
  def lengthOfLongestSubstring(self, s):
    # Fill this in.

print Solution().lengthOfLongestSubstring('abrkaabcdefghijjxxx')
    #10
```

## The Work

We want the solution in linear time which means we only want to loop the list once. Let's convert this over to go and get started.

```go
func lengthOfLongestSubstring(testString string) int {
	var prev string
	var count int = 0
	total := make([]int, 0)

	stringSlice := strings.Split(testString, "")

	for i, v := range stringSlice {
		if i == 0 {
			prev = ""
		} else {
			prev = stringSlice[i-1]
		}

		if prev == v {
			total = append(total, count)
			count = 1
		} else {
			count++
		}
	}
	sort.Ints(total)
	return total[len(total)-1]
}
```

Let's break this down: first we create `prev` to hold our previous value as we loop through the slice of characters. We then create a slice to hold the total counts as they get created.

Now we split the string into a slice of characters, then we loop that Slice of characters.

for the first character in the string, we have nothing to compare it against so to prevent from going out of index, we just set the value to a blank string `""`. After the first index, we want to take the previous character from the Slice. That character is just the index of the current character minus 1. Next, we take the previous value and compare it to the current value. If teh value is the same, we append the length of the `count` to our `total` slice, and reset the count to 1. Otherwise, we increment the count.

Once we're finish looping, we sort the array from smallest to largest (sorting gives us `n log n` time complexity) and return the last item from the slice.

The tricky part to this problem was actually an off-by-one error. I was stuck getting 9 as the longest substring and not 10. What I realized is that, while we're looping, we actually want the count and not the length of the numbers in the array before the count resets. That is to say, count starts at 1 where index starts at 0. We want count so when we reset, we reset to 1. 
