package main

import (
	"fmt"
	"sort"
	"strings"
)

func main() {
	testString := "abrkaabcdefghijjxxx"

	fmt.Println(lengthOfLongestSubstring(testString))
}

func lengthOfLongestSubstring(testString string) int {
	var prev string
	var count int = 0
	total := make([]int, 0)

	stringSlice := strings.Split(testString, "")

	for i, v := range stringSlice {
		if i == 0 {
			prev = ""
		} else {
			prev = stringSlice[i-1]
		}
		if prev == v {
			total = append(total, count)
			count = 1
		} else {
			count++
		}
	}

	sort.Ints(total)

	return total[len(total)-1]
}
