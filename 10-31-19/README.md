# 10-31-2019

It's Halloween today so traffic is lighter and there's a sense of excitement in the air. I'll be handing out full sized airheads, kitkats, and/or sour patch kids. I'm vying for the *coolest house on the block* title. Nonetheless, I think we'll pull our problem today directly from HackerRank.

HackerRank has the audacity to send nightly emails challenging you to answer one of their questions. It's definitely not a marketing ploy to get back you back on their site. If anything, it's a challenge against my manhood. Well, King's Knight to Bishop 4, checkmate, HackerRank. Let's do this.

Another off topic note here is I'm back to using my flat, second generation mac full-sized keyboard instead of my mechanical keyboard. I think I'm going to make the switch back. I like the low profile keys.

## The Question

Maria plays college basketball and wants to go pro. Each season she maintains a record of her play. She tabulates the number of times she breaks her season record for most points and least points in a game. Points scored in the first game establish her record for the season, and she begins counting from there.

For example, assume her scores for the season are represented in the array scores = [12, 24, 10, 24]. Scores are in the same order as the games played. She would tabulate her results as follows:

```text
                                 Count
Game  Score  Minimum  Maximum   Min Max
 0      12     12       12       0   0
 1      24     12       24       0   1
 2      10     10       24       1   1
 3      24     10       24       1   1
 ```

Given Maria's scores for a season, find and print the number of times she breaks her records for most and least points scored during the season.

Sample Input:

```text
9
10 5 20 20 4 5 2 25 1
```

Sample Output:

```text
2 4
```

## The Work

Ok, that was a lot. And, actually, the graph sort of confuses things. I get what they're trying to do but the min/max limits make you think there's a range to stay within. That's just not the case. I am looking for the number of times the lowest number is made lower, and the number of times the highest number is made higher strictly in order (because the order represents the games played.)

HackerRank always does this thing where they mention the length as the first digit (again, confuses things). I am just going to ignore that. So let's get started with the function signature:

```go
// breakingRecords calculates the number of times a record is broken
func breakingRecords(scores[] int32) [] int32 {

}
```

I take an array and return an array. Fun.

This seems like a basic < or > current value problem. I am sure there's some convenience methods inside on of the go standard libraries that could help to make this super easy but sticking with the pure language agnostic formats, I am going to start with:

```go
func main() {

	test := []int32{10, 5, 20, 20, 4, 5, 2, 25, 1}
	breakingRecords(test)

}

// breakingRecords calculates the number of times a record is broken
func breakingRecords(scores[] int32) [] int32 {
	timesBroken := make([]int32, 2)

	for _, v := range scores {
		fmt.Println(v)
	}

	return timesBroken
}
```

Notice the array created in the main function? That things weird. Golang strikes again with strange syntax. Makes sense but different than good 'ol JavaScript or Java. Ok, like our last episode, I am going to start with a basic loop and print to get a feel for things:

```text
10
5
20
20
4
5
2
25
1
```

Cool. Makes sense but something instantly pops out to me there: 20, 2x. Those HankerRank folks got jokes! Alright, let's go back to the problem statement and see what they tell us about what we should do in the case that the max or min is 2x'd. Ok, interpreting the sample input/output, we can see that they only increment counts when the scores are less than or greater than the current max/min. Also, another thing to note is the order of the resulting array: max AND THEN min. Cool. Back to the code. I am going to attack this with a simple 2 variable approach (capturing max and min).

The first result of the list is always going to be the max because there were no previous results.


```go
// breakingRecords calculates the number of times a record is broken
func breakingRecords(scores[] int32) [] int32 {
	timesBroken := make([]int32, 0)
	var totalMax, totalMin, max, min int32 = 0, 0, 0, 0

	for _, v := range scores {
		if v > max {
			max = v
			totalMax++
		}
		if v < min {
			min = v
			totalMin++
		}
	}

	timesBroken = append(timesBroken, totalMax, totalMin)

	fmt.Println(timesBroken)

	return timesBroken
}
```

Not pretty. Also, not correct. What I've been arguing with myself about is whether I need a `curr` variable to keep the the current value. I don't. Let's kick it back again:

```go
// breakingRecords calculates the number of times a record is broken
func breakingRecords(scores[] int32) [] int32 {
	timesBroken := make([]int32, 0)
	var totalMax, totalMin, max, min int32 = 0, 0, 0, 0

	for i, v := range scores {
		if i == 0 {
			max, min = v, v
		}
		if v > max {
			max = v
			totalMax++
		}
		if v < min {
			min = v
			totalMin++
		}
	}

	timesBroken = append(timesBroken, totalMax, totalMin)

	fmt.Println(timesBroken)

	return timesBroken
}
```

You know what? All I need is to instantiate the value of max and min with the first element of the array. We can do this in the loop but I'm going to elect to do it outside the loop so as to avoid the additional `if`


```go
// breakingRecords calculates the number of times a record is broken
func breakingRecords(scores[] int32) [] int32 {
	timesBroken := make([]int32, 0)
	var totalMax, totalMin, max, min int32 = 0, 0, scores[0], scores[0]

	for _, v := range scores {
		if v > max {
			max = v
			totalMax++
		}
		if v < min {
			min = v
			totalMin++
		}
	}

	timesBroken = append(timesBroken, totalMax, totalMin)

	fmt.Println(timesBroken)

	return timesBroken
}
```

Alright. Let's see what HackerRank has to say. The Println is printing the expected value.

Sweet! I got my third star. Gamification is so satisfying. Now, for the wrap up, let's inspect the O(n) time and space complexities:

This is going to be another linear times solution because the loop depends on the size of the initial array length. The space complexity is also O(n) because the amount of memory required is one-to-one with the length of the array while the variables remain constant (in terms of amount of memory).

I'll wrap up today by saying the very apropos "Cool". 
