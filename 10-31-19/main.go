package main

import(
	"fmt"
)


func main() {

	test := []int32{10, 5, 20, 20, 4, 5, 2, 25, 1}
	breakingRecords(test)

}

// breakingRecords calculates the number of times a record is broken
func breakingRecords(scores[] int32) []int32 {
	timesBroken := make([]int32, 0)
	var totalMax, totalMin, max, min int32 = 0, 0, scores[0], scores[0]

	for _, v := range scores {
		if v > max {
			max = v
			totalMax++
		}
		if v < min {
			min = v
			totalMin++
		}
	}

	timesBroken = append(timesBroken, totalMax, totalMin)
	return timesBroken
}
