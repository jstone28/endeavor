class ListNode(object):
  def __init__(self, x):
    self.val = x
    self.next = None

  # Function to print the list
  def printList(self):
    node = self
    output = ''
    while node != None:
      output += str(node.val)
      output += " "
      node = node.next
    print(output)

  # Iterative Solution
  def reverseIteratively(self, head):
    if head is None or head.next is None:
      return head

    prev = None
    current = head

    while current:
      # reference to next node
      next = current.next
      # set the current pointer to the previous node
      current.next = prev
      # set the previous node to the current node
      prev = current
      # set the current node to the next node
      current = next
    # set the new (reversed) head to None
    head = prev

  # Recursive Solution
  def reverseRecursively(self, head, tail=None):
    next = head.next
    head.next = tail
    if next is None:
      return head
    return self.reverseRecursively(next, head)


# Test Program
# Initialize the test list:
testHead = ListNode(4)
node1 = ListNode(3)
testHead.next = node1
node2 = ListNode(2)
node1.next = node2
node3 = ListNode(1)
node2.next = node3
testTail = ListNode(0)
node3.next = testTail

print("Initial list: ")
testHead.printList()
# 4 3 2 1 0
# testHead.reverseIteratively(testHead)
testHead.reverseRecursively(testHead)
print("List after reversal: ")
testTail.printList()
# 0 1 2 3 4
