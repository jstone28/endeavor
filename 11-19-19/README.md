# 11-19-2019

## The Problem

> Given a singly-linked list, reverse the list. This can be done iteratively or recursively. Can you get both solutions?

```text
Input: 4 -> 3 -> 2 -> 1 -> 0 -> NULL
Output: 0 -> 1 -> 2 -> 3 -> 4 -> NULL
```

## The Solution

This question is a classic. There are two methods typically used to solve this problem. Once is iterative and once is recursive. They offer the same time complexity, however, space-complexity is significantly different because the recursive method has to read all the nodes into memory before processing them.

Let's solve the iterative first:

so have a linked list that looks like this:

Input: `4 -> 3 -> 2 -> 1 -> 0 -> NULL`

Output: `NULL <- 4 <- 3 <- 2 <- 1 <- 0`

and each node is comprised of two attributes:

* `self.val` - indicating the value stored in that node within the linked list
* `self.next` - a pointer to the next method in the linked list

What we want to do is to flip the reference to reverse them:

`prev = None` - the previous node to the head is `None` so we set it.

`current = head` - we set the head to the current node; this is more for readability than anything

next we'll loop until we no longer have a `current` node

`next = current.next` is re
