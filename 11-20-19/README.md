# 11-20-2019

## The Problem

> Given a list of numbers with only 3 unique numbers (1, 2, 3), sort the list in O(n) time.

```go
Input: [3, 3, 2, 1, 3, 2, 1]
Output: [1, 1, 2, 2, 3, 3, 3]
```
## The Solution
