package main

import(
	"fmt"
)


func main() {
	t := []int{3, 3, 2, 1, 3, 2, 1}
	// [1, 1, 2, 2, 3, 3, 3]
	fmt.Println(nSort(t))
}

func nSort(intArray []int) []int {
	low := 0
	high := len(intArray)-1
	mid := 0

	for mid <= high {
		if intArray[mid] == 0 {
			intArray[low], intArray[mid] = intArray[mid], intArray[low]
			low = low + 1
			mid = mid + 1
		} else if intArray[mid] == 1 {
			mid = mid + 1
		} else {
			intArray[mid], intArray[high] = intArray[high], intArray[mid]
			high = high - 1
		}
	}

	return intArray
}
