package main

import (
	"fmt"
)

func main() {
	arr := []int{4, 3, 2, 4, 1, 3, 2}
	fmt.Println("1 should equal: ", findNonDuplicate(arr))
}

func findNonDuplicate(arr []int) int {
	intMap := make(map[int]int)
	totalMap := make(map[int]int)
	var count int

	for _, v := range arr {
		count = 0
		if intMap[v] != 0 {
			count = 1
		}
		total := count + 1
		intMap[v] = total
		totalMap[total] = v
	}
	return totalMap[1]
}
