# 11-22-2019

## The Problem

> Given a list of numbers, where every number shows up twice except for one number, find that one number.

```go
Input: [4, 3, 2, 4, 1, 3, 2]
Output: 1
```

*Challenge: Find a way to do this using O(1) memory.*


## The Solution


This comes from another daily interview question email. I think it might be my favorite thus far. Initially, I loaded everything into a single map and then looked at that map, but the look up was costly (in terms of performance) so, after reading some comments on stackoverflow regarding value-based map look ups, I cam across some advice to reverse the map. Thinking alog those lines, I created two maps. The first one stores the slice integers and their count. The second inverts that and stores the total count and the key secondarily.

I made the maps of the same time `[int]int`, however, if I wanted to be more concise, I would make the total count map `[int][]int` making it able to store all the values with duplicates.

We simply return the value pair for the key `1`. 
