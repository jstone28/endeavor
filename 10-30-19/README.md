# 10-30-2019

It's a muggy and rainy day just before Halloween and I'm sitting in a mostly put-together office watching the rain increase in intensity.

Today's question comes from Glassdoor. I was poking around there looking and came upon an interview question so simple, it fit into < one sentence. Must be easy! Let's give it a go.

*note* I'm not entire sure this is what the author of this question intended because it's the internet and the person who wrote it, wrote it more like `addSubtract((1)(2)(3)(4))(5)` so I'm not all that sure but I think my version is entertaining enough.


## The Question

> Given an array of integers, alternate between adding and subtracting

Example:

you're given `[1, 2, 3, 4 , 5]` you turn that into: `1+2-3+4-5` and return the result.


## The Work

Ok, let's do a few things before getting started. Let's draw out a picture. This inteview advice came from the [TechLead](https://www.youtube.com/channel/UC4xKdmAXFh4ACyhpiQ_3qBw) on Youtube (much recommended). The reason for the picture is because it makes it easy to refer back to. Also because writing code is just an extension or, perhaps, a level abstracted from the actual problem so the advice stands: solve the problem first THEN write the code.

Ok, let's look again:

`[1, 2, 3, 4 , 5]` you turn that into: `1+2-3+4-5`

It seems pretty straight forward. As a friend of mine once opined: "a function is just the relationship between two domains." And, despite the fact that it took my forever to understand what he was saying, it makes sense now.

Our two domains are:

`[1, 2, 3, 4 , 5]`

and

`1+2-3+4-5`

What's the relationship? Well, the first thing that strikes me is that `i` or the index of the problem maps directly to the symbol (positive or negative). Specifically, the relationship is *for every odd index, use an addition sign. for every even index, use a subtraction symbol. Fair enough. I'll also need to loop this array because I need to add the numbers together.

I think the best way to do is to probably set value (positive or negative) depending on its relation to its index, and then add that number to a total that's tracked through the life of the loop (so that we're not double looping with a crazy O().

Let's start with some testing:

```go
	c := make([]int, 5)
	c[0] = 1
	c[1] = 2
	c[2] = 3
	c[3] = 4
	c[4] = 5
```

There's a slice (golang's version of an extendable array or arraylist) to work from. Next let's create the loop:

```go
	for i, v := range c {
    fmt.Println(i)
    fmt.Println(v)
	}
```

Amateur hour here, I know but I'm just going to print the index and value so I can look at them in the output.

```go
index: 0
value: 1
index: 1
value: 2
2
3
3
4
4
5
```

Alrighty; got that out of the way and everything compiles. Let's get to the meat of this:

```go
for i, v := range c {
		if i % 2 != 0 {
			total += -v // make the number negative
		} else {
			total += v
		}
	}
}
```

I'm going to loop over the slice. If the index is **not** divisible by two (noted by the modulo 2) then I'm going to negate the value and add it to our total (virtually the same thing as subtracting). In the case that it is divisible by two, I am just going to add that to the total.

What that ends up looking like is this:

`[1, 2, 3, 4 , 5]` --> `+1 -2 +3 -4 +5`


The trick here is that the 0 index will be accidentally negated because `0 % 2= 0` so we can create an if for that:

```go
for i, v := range c {
  if i == 0 {
    total += v
  } else if i % 2 == 0 {
    total += (-v) // make the number negative
  } else {
    total += v
  }
}
fmt.Println(total)
```

ok, cool. We get -1  and according to my alfred calculator, that's what we were looking for.

Just for fun, let's move the print statement up and watch the progression:


```go
total @ index[0] - 1
total @ index[1] - 3
total @ index[2] - 0 // 3 + (-3)
total @ index[3] - 4
total @ index[4] - -1 // 4 + (-5)
```

The big O of this function will be linear time O(n) because we're looping over a slice and the length of that loop is dependent on the length of the variable slice.

Upon thinking about this further, it seems like a variation on the old fizzbuzz problem where the asker is looking for module use to determine divisibility. Additionally, if you try inverting the logic, say:

```go
for i, v := range c {
 if i % 2 != 0 {
    total += (-v) // make the number negative
  }
    total += v // just add
}
fmt.Println(total)
```
You still end up with the initial problem at index 0 and 1. Index 0 % 2 is 0 and thus the value at that index is transformed into a negative. Index 1, presumably the first number we want to subtract is added because 1 % 2 = 1. 
