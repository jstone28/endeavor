package main

import(
	"fmt"
)

// given an array of integers, alternate between adding and subtracking
// 1 + 2 - 3 + 4 - 5 == -1
func main() {
	var total int = 0
	c := make([]int, 5)
	c[0] = 1
	c[1] = 2
	c[2] = 3
	c[3] = 4
	c[4] = 5

	for i, v := range c {
		// 0 % 2 == 0 always; skip that one
		if i == 0 {
			total += v
		} else if i % 2 == 0 {
			total += (-v) // make the number negative
		} else {
			total += v
		}
	}
	// print resulting total
	fmt.Println(total)
}
