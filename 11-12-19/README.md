# 11-12-19

## The Problem

Given an integer, `N`, find and print the number of letter `a`'s in the first N letters of Lilah's infinite string.

For example, if the string `s=abcac` and `N=10` , the substring we consider is `abcacabcac`, the first `10` characters of her infinite string. There are `4` occurrences of a in the substring.


# The Work

Ok, unlike the first time I read this problem, what's actually happening is that we want a string of characters with a length `N`. I won't confuse you with what I thought it was saying, but now that I understand what it wants, it looks somethingn like this

Inputs:

`N = 10`
`s = "abcca`

Constructed:

`abcacabcac`

Output:

`4`

To get this to work, we'll need to break the substring `s` into an array, and loop that for the number of `a`'s. If we look closer there's actually an equation we can use here:

Let's see how many times the string `s` can be distributed into `N`:

`var x int64 = n / int64(len(s))`

This will return a whole number and ignore the remainders so we'll get those next

`var leftovers = int(n) % len(s)`

The length N mod len(s) give us the remainder function of the previous calculation. This will help us decide how many additional `a`'s we need to add.

Let's create our loop:

```go
	var total, add int64 = 0, 0

	for i, v := range strings.Split(s, "") {
			if v != "a" {
					continue
			}
			if i < leftovers {
					add++
			}
			total++
  }

  return (total * x) + add
```

We're going to loop over the passed in substring split into an array of characters. If the current value is not an `a` we'll continue to the next loop. If the current index is less than the result of the remainder function from the previous step, we'll add this to another variable `add` for appending to the return value. Bear in mind, also, that we're only counting a's in the string.

The relationship between `leftovers` and the index is the number of characters in the substring that are *leftover* from the number of whole times the length `N` can be divided by the substring. Because our loop is only counting `a`s, we're only incrementing `add` when we find an `a`. 

Lastly, we increment the total because we found an `a`

The final calculation boils down to:

total number of a's in a single substring `*` number of times that substring is repeated with `N` the required length. Finally, we find the length of the partial string, and if there are a's to be added there, we'll add those too.

That gives us the total.
