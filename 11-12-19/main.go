package main


import(
	"fmt"
	"strings"
)


func main() {
	n := int64(10)
	s := "abcac"

	fmt.Println(repeatedString(s, n))

}

// Complete the repeatedString function below.
func repeatedString(s string, n int64) int64 {
	var x int64 = n / int64(len(s))
	var leftovers = int(n) % len(s)
	var total, add int64 = 0, 0

	for i, v := range strings.Split(s, "") {
			if v != "a" {
					continue
			}
			if i < leftovers {
					add++
			}
			total++
	}
	return (total * x) + add
}
