package main


import(
	"fmt"
	"sort"
)

func main(){
	a := []int32{3, 2, 1, 2, 3}
	fmt.Println(findMinimumDistance(a))
}

func findMinimumDistance(intArr []int32) int32 {
	distance := make([]int, 0)
	var length int = 0
	for i := int32(0); i < int32(len(intArr)); i++ {
			for j := int32(len(intArr)-1); j >= 0; j-- {
					if intArr[i] == intArr[j] {
							length = int(i - j)
							if length > 0 {
									distance = append(distance, length)
							}
					}
			}
	}

	if len(distance) != 0 {
			sort.Ints(distance)
			return int32(distance[0])
	}
	return -1
}
