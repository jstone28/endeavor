# 11-17-10

## The Problem

> We define the `minimum` distance between two array values as the number of indices between the two values. Given A, find the minimum distance between any pair of equal elements in the array. If no such value exists, print -1.

Example:

a = [3, 2, 1, 2, 3]

3's match and 2's match

3's are at a[0] and a[4]
4 - 0 = 4

2's are at a[1] and a[3]
3 - 1 = 2


## The Work

This solution involve looping the list forward and backwards, and comparing the items in flight. If we find two matching values, we take their respective indices and subtract them. If the length is greater than 0, we append it to our distance array. We do this because 0 is the length for non-matching values, and because the two arrays will run to completion, we'll end up with both a positive and negative instance of the length (so we can just take the positive one).

If there are any matching characters then we take the array and sort it. The first value in the sorted slice is the minimum length. 
