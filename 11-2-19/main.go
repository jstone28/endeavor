package main

import (
	"fmt"
)

func main() {
	l := []int{1, 2, 3, 5, 6, 7}
	fmt.Println(findFirstMissing(l))

}

func findFirstMissing(l []int) int {
	for i := 1; i <= len(l); i++ {
		if l[i-1] != i {
			return i
		}
	}
	return 0
}
