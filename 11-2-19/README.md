# 11-2-19

The day before the end of daylight saving's time. The morning is dark until about 8am and it's still light around 6pm. Tomorrow that's going to invert and it's ways fun to watch chaos ensue, internally, in myself: "oh, it's so dark and it's only 4pm".

## The Next Couple of Problems

Today's problem is going to come from a random search of interview questions. There's a few in here I want to do. I'm going to try and pull the most obvious (to be asked) of the bunch -- maybe 2 or 3. Definitely one in data structures and definitely one in algorithms, and maybe one in string manipulation just for the fun of it. Here we go:


### Problem 1

Given a list of integers, find the first number missing from the set

```text
l = [1 ,2 ,3, 5, 6, 7]
expected: 4
```

## The Work

We're going to take advantage of the fact that our loop is going to loop, iteratively from the 0th index to the 6th. There's small gotcha here because we want to use our if to test the 0th element so, instead of starting at 0, we'll start at `i := 1` and we'll also loop the full length of the array (because we'll be subtracting 1 in the check -- that'll save guard us from going out of bounds)

```go
func findFirstMissing(l []int) int {
	for i := 1; i <= len(l); i++ {
		if l[i-1] != i {
			return i
		}
	}
	return 0
}
```

With that, we end up with this. The thing to note here is that, instead of returning, we could append the numbers to a list and return the list or we can just return the first non-sequential number. This question has a couple forms so it's good to know both.


## Resource
[Stody Guide](https://medium.com/better-programming/the-software-engineering-study-guide-bac25b8b61eb)
