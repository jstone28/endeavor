# 11-16-19

## The Problem

> Imagine you are building a compiler. Before running any code, the compiler must check that the parentheses in the program are balanced. Every opening bracket must have a corresponding closing bracket. We can approximate this using strings.

Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:
* Open brackets are closed by the same type of brackets.
* Open brackets are closed in the correct order.
* Note that an empty string is also considered valid.

```python
Input: "((()))"
Output: True

Input: "[()]{}"
Output: True

Input: "({[)]"
Output: False
```


## The Work

My first attempt at this was a naive implementation using passed in values to check continuity in the string. While this solution does check the balance. It doesn't check the correct order which is a requirement of this problem. The naive implementation is as follows:

```go
func checkStringFormat(str string) bool {
	// create the character slice
	charSlice := strings.Split(str, "")
	if len(charSlice) == 0 {
		return true
	} else if len(charSlice) % 2 != 0 {
		return false
	}


	// create the character value map
	charMap := make(map[string]int, 0)

	charMap["("] = 1
	charMap[")"] = -1
	charMap["["] = 1
	charMap["]"] = -1
	charMap["{"] = 1
	charMap["}"] = -1

	var parens, braces, brackets int = 0, 0, 0
	for _, v := range charSlice {

		val := charMap[v]

		if v == "(" || v == ")" {
			parens += val

		} else if v == "{" || v == "}" {
			braces += val

		} else if v == "["  || v == "]" {
			brackets += val
		}

		if parens < 0 || braces < 0 || brackets < 0 {
			return false
		}
	}
	return true

}
```

We'll move on to checking order along with balance in this next solution which requires the use of a data structure, specifically a stack, to keep track of the order of items.

Interestingly enough, Go doesn't have a default implementation of a stack or a queue, for that matter. So I meditated on that a bit. When you think about it. Most (if not all data structures -- I have to think on this more) are an abstraction on your basic slice/extend-able array; having the core data types of the language be: array, slice, map, and struct makes perfect sense.

Let's use a slice as a stack.

Now we've got it implemented and passing. The issue I ran into initially was that I wasn't popping off the characters from the end of the slice; rather, I was just calling the last character and comparing it. That messes things up because, at the end of the function, the slice should be empty if the string is balanced. 
