package main


import(
	"fmt"
	"strings"
)


func main() {
	// s := "()(){(())" // false
	// s := "((()))" //true
	// s := "[()]{}" // true
	// s := "({[)]" // false
	// s := "" // true
	// s := "[{(})]" // false
	fmt.Println(checkStringFormat(s))
}

func checkStringFormat(str string) bool {
	// create the character slice
	charSlice := strings.Split(str, "")

	// house keeping; if the len is 0 or not even return
	if len(charSlice) == 0 {
		return true
	} else if len(charSlice) % 2 != 0 {
		return false
	}

	// create the stack (as a slice)
	balance := make([]string, 0)
	var t string
	for _, v := range charSlice {
		// if opening, pushing on to stack
		if v == "{" || v == "(" || v =="[" {
			balance = append(balance, v)
		}

		// if the stack is empty then we have  closing without opening
		if len(balance) == 0 {
			return false
		}

		if v == "}" {
			t = balance[len(balance)-1]
			balance = balance[:len(balance)-1]
			if t != "{" {
				return false
			}
		} else if v == ")" {
			t = balance[len(balance)-1]
			balance = balance[:len(balance)-1]
			if t != "(" {
				return false
			}
		} else if v == "]" {
			t = balance[len(balance)-1]
			balance = balance[:len(balance)-1]
			if t != "[" {
				return false
			}
		}
	}
	if len(balance) != 0 {
		return false
	}
	return true
}
