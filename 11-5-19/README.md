# 11-5-19

We're back today with an interview prep problem. This one is quite well known so it needs no introduction. Let's get started:

## The Problem

```text
Given a 2d array, find the spiral order of that array
Input:
[
 [ 1, 2, 3 ],
 [ 4, 5, 6 ],
 [ 7, 8, 9 ]
]
Output: [1,2,3,6,9,8,7,4,5]
```

This test case was lifted directly from [leetcode](https://leetcode.com/problems/spiral-matrix/). Check them out for a working example.

## The Solution

The basics of this problem lie in a couple of key concepts. Namely, that we'll want to move in all 4 cardinal directions, and that we won't want to traverse the same array twice.

If we continue on that path, we'll end up with a spiral.

I believe there is a recursive solution to this problem, however, I am going to use an iterative one. The primary reasons for that are readability and memory protection.

Let's get the main setup:

```go
func main() {
	input := [][]int{
		{ 1, 2, 3 },
		{ 4, 5, 6 },
		{ 7, 8, 9 },
	}

	m := 3
	n := 3

	fmt.Println(input)
}
```

That gives us an array like this:

```go

```

Which is what we're looking for.
