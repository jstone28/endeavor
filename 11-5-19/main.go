package main

import (
	"fmt"
)

func main() {
	matrix := [][]int{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
	}

	m := 3
	n := 3

	fmt.Println(spiralOrder(m, n, matrix))
}

// m the number of rows
// n the number of columns
// matrix the 2d array
func spiralOrder(m int, n int, matrix [][]int) []int {
	spiralOrdered := make([]int, 0)
	// define the boundaries
	top := 0
	bottom := m - 1
	left := 0
	right := n - 1
	direction := 0

	for top <= bottom && left <= right {
		if direction == 0 {
			for i := left; i <= right; i++ {
				spiralOrdered = append(spiralOrdered, matrix[top][i])
			}
			top++
			direction = 1
		} else if direction == 1 {
			for i := top; i <= bottom; i++ {
				spiralOrdered = append(spiralOrdered, matrix[i][right])
			}
			right--
			direction = 2
		} else if direction == 2 {
			for i := right; i >= left; i-- {
				spiralOrdered = append(spiralOrdered, matrix[bottom][i])
			}
			bottom--
			direction = 3
		} else if direction == 3 {
			for i := bottom; i <= top; i-- {
				spiralOrdered = append(spiralOrdered, matrix[i][left])
			}
			left++
			direction = 4
		}
		if direction%4 == 0 {
			direction = 0
		}
	}
	return spiralOrdered
}
