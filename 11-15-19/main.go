package main

import(
	"fmt"
)

func main() {
	s := "tracecars"
	fmt.Println(longestPalindrome(s))
}

func longestPalindrome(pal string) string {
	var sub string = ""
	end := len(pal) - 1
	if len(pal) < 1 {
		return pal
	}
	for i:=0; i < len(pal); i++ {
		if pal[i] == pal[end - i] {
			sub += string(pal[i])
		}
	}

	return sub
}
