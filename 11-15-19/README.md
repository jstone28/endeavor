# 11-15-2019


## The Problem

> A palindrome is a sequence of characters that reads the same backwards and forwards. Given a string, s, find the longest palindromic substring in s.

```python
# Test program
s = "tracecars"
print(str(Solution().longestPalindrome(s)))
```

## The Work

Palindromes mean the same front as back, therefore, we could create two pointers (one at the front of the array and one at the end) and loop them until we find matching set of pointers. We start storing the substring until we find mismatching pointers.

We can complete this with a single for loop and therefore the search is O(n). The space complexity is O(n) as well because we're setting our variables at the start, and not further generating them. 
