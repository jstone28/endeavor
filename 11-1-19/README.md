# 11-1-2019

Oof. Double Oof. I spent yesterday in the depths of a question from HackRank. The problem was stated kind of oddly (at least, that's my perception). I spent at least two hours redefining the question to make sense of it. And then another 2 hours implementing various solutions for both. It was a nightmare on Halloween. But I digress. I left that question in a branch that I might merge in at some point. There's a lot of text but also a lot of *noodling*

As for another digression, strange that I just realized this, but a good way to read a traditional for loop is to say

```go
	for i := len(a) - 1; i >= 0; i-- {
		b = append(b, a[i])
  }
```

> Set `i` to the end of the array, if `i` is `>= 0`, decrement `i` by 1

I think that's CS 101 type stuff or even way way before that but, for some reason, the if part of the middle statement never dawned on me. The context I'm specifically speaking to is, for example, `i >= 0` loops there but `i == 0` does not. That's because, at the first loop, `i` is the last index of the array. And the last index of the array (in this case), is not equal to zero so everything stops.

Ok so back to your regularly scheduled program

## The Problem

Let's try one that came out of yesterday's thinking. And, spoiler, I had to look it up. But today, I'm back to implement it from scratch:

> Given an array of integers, find all permutations of the array

This question has absolutely stumped me for the last two days, and I think I've finally realized why. My definition of permutation and combination have been wrong. Or, well, incorrect. Elementary math says "hello". Time to swallow that pride.

> Combination: a combination is a selection of items from a collection, such that (unlike permutations) the order of selection does not matter. [from wikipedia where all can be trusted]

> Permutation: permutation is the act of arranging the members of a set into a sequence or order, or, if the set is already ordered, rearranging (reordering) its elements—a process called permuting [from wikipedia]

The difference? Wikipedia captures that too:

> Permutations differ from combinations, which are selections of some members of a set regardless of order.

Ok, so maybe an exaggeration. In fact, the last statement captures where I was getting stuck. If, in fact, my thinking is correct: given a set of (1, 2, 3) then a permutation of that set would be (1) alone because you don't need all the numbers in the set to make it a valid permutation.

Ah hah! Confirmed:

> Permutations and combinations, the various ways in which objects from a set may be selected, generally without replacement, to form subsets. This selection of subsets is called a permutation when the order of selection is a factor, a combination when order is not a factor. [Britannica]

So I guess I should pause for a moment, and say these are the types of questions that absolutely floor me in interviews. I can calculate permutations just fine BUT when permutations, by definition, also contain subsets then things get a little crazy. The onus is really on me. I should clarify:

> of length l (the length of the set), right?

Note to self.

Ok, so let's clarify the question to say:


> Given an array of integers, find all permutations of the array of length l. The length of the array

Finally.

## The Work

Ok, so my search last night ended up on [Heap's Algorithm](https://en.wikipedia.org/wiki/Heap%27s_algorithm) which is the industry agreed upon best solution to this problem.

Let's start with a main to give us some ground:

```go
func main() {
  	a := []int{1, 2, 3}
  	permutation(a)
}
```

Ok, let's implement `permutation()` but first, let's think about what we're looking for:

```text
1,2,3
3,2,1
```

OoOof. Ok, my brain has hit the wall. Time to start breaking these problems up and reference the internet (listed below)

Let's start with heap algorithm. Let's try and sum that up in a few sentences:

We can borrow directly from wikipedia:

> The algorithm minimizes movement: it generates each permutation from the previous one by interchanging a single pair of elements; the other n−2 elements are not disturbed.

And we can confirm that by watching how this set moves:

```text
[1 2 3]
[2 1 3]
[3 1 2]
[1 3 2]
[2 3 1]
[3 2 1]
```

It's taken me a while to get my head wrapped around this solution. I'll have to take it apart further:

```go
func main() {
	a := []int{1, 2, 3}
	permutation(a, len(a))
}

func permutation(a []int, length int) {
	if length == 1 {
		fmt.Println(a)
	}
	for i := 0; i < length; i++ {
		permutation(a, length-1) // recursive call
		if length%2 == 1 {
			a[0], a[length-1] = a[length-1], a[0]
		} else {
			a[i], a[length-1] = a[length-1], a[i]
		}
		fmt.Println(length)
	}
}
```
And here's the output:


```text
[1 2 3]
[2 1 3]
[3 1 2]
[1 3 2]
[2 3 1]
[3 2 1]
```

The pattern is indices 0 and 1 switch, then 0 and 2 switch, then 0 and 1 switch then 0, 2 switch until we have all the available permutations at length l. 

[Solution resource](https://gist.github.com/manorie/20874a3c59e9fdfb4e184cac4130944d)
[Permutation resource](https://www.cs.colorado.edu/~yuvo9296/courses/csci2824/sect26-PermComb.html)

TIL: a (traditional) for-loop as has three parts. All three are optional: the initialization, condition, and afterthought.

initialization: creates any required variables (of the same type)
condition: checks on each loop whether to continue (halting mechanism)
afterthought: an action performed after every loop (usually an increment or decrement)
