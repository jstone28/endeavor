package main

import (
	"fmt"
)

func main() {
	a := []int{1, 2, 3}
	permutation(a, len(a))
}

func permutation(a []int, length int) {
	if length == 1 {
		fmt.Println(a)
	}
	for i := 0; i < length; i++ {
		permutation(a, length-1) // recursive call
		if length%2 == 1 {
			a[0], a[length-1] = a[length-1], a[0]
		} else {
			a[i], a[length-1] = a[length-1], a[i]
		}
	}
}
