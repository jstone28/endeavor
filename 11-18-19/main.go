package main


import(
	"fmt"
	"sort"
)

type set struct {
	m map[int]struct{}
}


func main() {
	// arr := []int{4, 2, 9, 7, 90, 89, 3}
	arr := []int{1, 2, 9, 7, 90, 89, 3}

	fmt.Println(longestConsecutive(arr))
}


func longestConsecutive(arr []int) int {
	// create map
	arrMap := make(map[int]int)
	s := &set{}
	s.m = make(map[int]struct{})


	// slice containing lengths
	lengthSlice := make([]int, 0)
	total := make([]int, 0)

	var next, prev int = 0, 0
	// for loop the list
	for _, v := range arr {
		arrMap[v] = 1
		next = v + 1
		prev = v - 1
		for arrMap[next] != 0 {
			lengthSlice = append(lengthSlice, next)
			next++
		}
		for arrMap[prev] != 0 {

			lengthSlice = append(lengthSlice, prev)
			prev--
		}
		total = append(total, len(lengthSlice))
		fmt.Println(total)
	}

	if len(total) != 0 {
		sort.Ints(total)
		return total[len(total)-1]
	}
	return 0
}
