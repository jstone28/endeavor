# 11-18-19

## The Problem

> Given an array of number find the longest set of consecutive numbers in the list

For example:

[1, 2, 9, 7, 90, 89, 3]

[1, 2, 3] - longest
[98, 90]

## The Work

The O(n^2) solution to this; or the brute force solution is to iterate over the numbers in the array, when you come upon a number check the number above it and below it for matching, if there's more matching, check again. If not, stop and move on to the next number.

But we can do this more intelligently with a specific data structure: a hashset.

* What we'll do is look at the first number in the list
* If a number + 1 exists, we'll + 1 and continue
* If a number - 1 exists, we'll - 1 and continue
* Check the total length list to see if it has data. If it does, we'll sort it and take the last item (MAX)

Implementing this has made me realize why python is so commonly chosen for these types of exercises (whiteboarding). It really comes down to readability and default implementations; for example, of set and hashset.

 This issue we run into here is that we're consistently off by few because we're not marking numbers as `checked` after we've used them.


## WIP

Need to mark elements as checked before moving on. 
